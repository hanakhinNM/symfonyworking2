<?php

namespace App\DataFixtures;

use App\Entity\Auteur;
use App\Entity\Book;
use App\Entity\Article;
use App\Entity\Genre;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class AppFixtures extends Fixture
{
    /**
     * @var UserPasswordHasherInterface
     */
    private $pass_hasher;

    public function __construct(UserPasswordHasherInterface $passwordHasher)
    {
        $this->pass_hasher = $passwordHasher;
    }


    public function load(ObjectManager $manager): void
    {
        // $product = new Product();
        // $manager->persist($product);
        $faker = Faker\Factory::create("fr_FR");

        // génération d'utilisateur
        $userone = new User();
        $userone->setNom("Legrand")->setPrenom("Alexandre")
            ->setEmail("alexandre.legrand@greek.com")
            ->setPassword($this->pass_hasher->hashPassword($userone, "demo"));
        $manager->persist($userone);

        $usertwo = new User();
        $usertwo->setNom("khan")->setPrenom("Gengis")
            ->setEmail("atila@mongolie.com")
            ->setPassword($this->pass_hasher->hashPassword($usertwo, "demo"));
        $manager->persist($usertwo);

        // ajout des genres litteraire
        $categories = ['Science Fiction','Roman biographique', // tableau de genre
            'Polar', 'Roman historique','Essai'];
        $u = 0;
        foreach ($categories as $category){ // parcour du tableau de genre
            $genre = new Genre();
            $genre->setCategory($category);

            $this->setReference("genre-".$u, $genre);

            $manager->persist($genre);
            $u++;
        }

        // creation des auteurs
        $auteurNum = [];
        for($i = 0; $i < 30; $i++){
            $auteur = new Auteur();
            $auteur->setNom($faker->lastName)
                ->setPrenom($faker->firstName)
                ->setAnniversaire(new \DateTime($faker->date("Y-m-d", "2008-01-01")));
            $this->setReference("auteur-".$i, $auteur);
            array_push($auteurNum, $i);
            $manager->persist($auteur);
        }

        // ajout de 50 livres
        for ($i = 0; $i <50; $i++){
            $book = new Book();

            // récuparation de l'objet

            // rand(0, 4)
            //$genre = $this->getReference("genre-".$faker->numberBetween(0, count($categories)-1));
            $genre = $this->getReference("genre-".rand(0,count($categories)-1));
            $nbr = rand(1,5); // definition du nombre d'auteur

            $book->setTitre($faker->sentence(6,true))
                ->setDescription($faker->paragraph(5, true))
                ->setNotation($faker->numberBetween(1,10))
                ->setGenre($genre)
                ->setDatePub(new \DateTime($faker->date("Y-m-d", 'now')));
            $randomNumber = array_rand($auteurNum, $nbr);
            $tab = [];
            if($nbr == 1){
                $tab[0]= $randomNumber;
            } else {
                $tab = $randomNumber;
            }
            for ($z = 1; $z <= $nbr; $z++){
                $book->addAuteur(
                    $this->getReference("auteur-" . $tab[$z-1])
                );
            }

            $manager->persist($book);
        }

        // ajouter 30 articles
        for ($i = 0; $i <30; $i++)
        {
            $article = new Article();
            $article->setTitre($faker->sentence(6,true))
                ->setAuteur($faker->name)
                ->setContent($faker->paragraph(10,true))
                ->setDateArt(new \DateTime($faker->date("Y-m-d",'now')));
            $manager->persist($article);
        }

        $manager->flush();


    }
}
