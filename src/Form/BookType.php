<?php

namespace App\Form;

use App\Entity\Book;
use App\Entity\Genre;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class BookType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('titre', null,[
                'label' => "Titre de l'oeuvre"
            ])
            ->add('datePub', null, [
                'label' => "Date de publication",
                'widget' => "single_text",
                "years" => range('1900', '2022'),
            ])
            ->add('description')
            ->add('notation')
//            ->add('auteurs')
            ->add('genre', EntityType::class, [
                'label' => 'Genre litteraire',
                'class' => Genre::class,
                'placeholder' => "Choisir un genre",
                'choice_label' => "category",
                'multiple' => false
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Book::class,
        ]);
    }
}
