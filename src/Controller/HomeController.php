<?php

namespace App\Controller;

use App\Entity\Article;
use App\Entity\Auteur;
use App\Entity\Book;
use App\Form\AuteurType;
use App\Form\BookType;
use App\Repository\ArticleRepository;
use App\Repository\AuteurRepository;
use App\Repository\BookRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{

    /**
     * @var \string[][]
     */
    private $articles = [
        ['titre'=>'Article 1', 'resume'=>'résumé article 1'],
        ['titre'=>'Article 2', 'resume'=>'résumé article 2'],
        ['titre'=>'Article 3', 'resume'=>'résumé article 3'],
    ];
    /**
     * @var ArticleRepository
     */
    private $artRepo;

    /**
     * @var BookRepository
     */
    private $bookRepo;

    /**
     * @var AuteurRepository
     */
    private $autRepo;

    public function __construct(ArticleRepository $articleRepository,
                                BookRepository $bookRepository,
                                AuteurRepository $auteurRepository)
    {
        $this->artRepo = $articleRepository;
        $this->bookRepo = $bookRepository;
        $this->autRepo = $auteurRepository;
    }


    /**
     * @Route("/home", name="home", methods={"GET"})
     * @return Response
     */
    public function welcome()
    {
        // je déclare un tableau d'argument
        $tableau = [1, 2, "abc"];
        return $this->render("front/home.html.twig", [
            "tableau" => $tableau,
            "titre" => "un titre de bienvenue", // je déclare une string
        ]);
//        return new Response(
//            "<html><head><title>Bienvenue</title></head><body></body></html>"
//        );
    }

    /**
     * @Route("/page/{numPage}", name="app_page", methods={"GET"}, requirements={"numPage"="\d+"})
     * @param string $numPage
     * @return Response
     */
    public function pages( int $numPage = 1 )
    {
      return $this->render("front/page.html.twig", [
         "numPage" => $numPage,
      ]);
    }

//    public function pageDeux()
//    {
//        return new Response(
//            "<html><head><title>Page2</title></head><body><h2>Vous êtes sur la page 2</h2><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium alias commodi deserunt eius eligendi esse facere, impedit labore nemo placeat repudiandae tenetur, ut? Ea illo, molestiae officia quo similique voluptate.</p></body></html>"
//        );
//    }

    /**
     * @Route("/page/{slug}", name="app_slug", methods={"GET"})
     * @param string $slug
     * @return void
     */
    public function autrePage(string $slug)
    {
        return new Response(
            "<html>
                        <head>
                            <title>Page $slug</title>
                        </head>
                        <body>
                        <h2>Vous êtes sur la page $slug</h2>
                        <p><a href='/home'>retour</a> </p>
                        <p>Une autre page</p></body></html>"
        );
    }

    /**
     * @Route("/listeArticles", name="listeArticles", methods={"GET"})
     * @return void
     */
    public function listeArticles()
    {
//        $articles = $this->getDoctrine()->getRepository(Article::class)
//            ->findAll();
        $articles = $this->artRepo->findAll();
        // dump($articles); // var dump visuel
        return $this->render("front/template_part/_listeArticles.html.twig",[
            'articles' => $articles,
        ]);
    }

    /**
     * @Route("article/{numArt}", name="article", methods={"GET"}, requirements={"numArt"="\d+"})
     * @param int $numArt
     * @return Response
     */
    public function article(int $numArt)
    {
//        $article = $this->getDoctrine()->getRepository(Article::class)
//            ->find($numArt);
        $article = $this->artRepo->find($numArt);
        return $this->render("front/article.html.twig",[
            'article'=> $article
        ]);
    }

    /**
     * @Route("/articlesFiltres", name="articlesFiltres", methods={"GET"})
     * @return Response
     */
    public function articlesFiltre(Request $request)
    {
        $dateLimite = $request->get("datelimit");
//        dump($dateLimite);
        $articles = $this->artRepo->findByDateSup($dateLimite);
//        dump($articles);
        return $this->render("front/listeFiltre.html.twig", [
            'articles' => $articles,
        ]);
    }

    /**
     * @Route("/latest", name="latest", methods={"GET"})
     * @return Response
     */
    public function listLatest()
    {
        $articles = $this->artRepo->findLatest();
        return $this->render("front/listeFiltre.html.twig", [
            'articles' => $articles,
        ]);
    }


    /**
     * @Route("/books", name="listeBooks", methods={"GET"})
     */
    public function listeBook()
    {
        $books = $this->bookRepo->findAll();
        return $this->render("front/listeBook.html.twig",[
           'books' => $books,
        ]);
    }

    /**
     * @Route("/editBook/{id}", name="bookedit")
     * @return void
     */
    public function editBook(int $id, Request $request)
    {
        if($id == -1){ // si id == -1 nouveau livre
            $livre = new Book();
        } else {
            $livre = $this->bookRepo->find($id);
        }
        $form = $this->createForm(BookType::class, $livre);

        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $em = $this->getDoctrine()->getManager();
            $em->persist($form->getData());
            $em->flush();
            return $this->redirectToRoute('listeBooks');
        }

        return $this->render("front/form/editBook.html.twig",[
            'livre' => $livre,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/deleteBook/{id}", name="deletebook")
     * @param int $id
     * @param Request $request
     * @return void
     */
    public function deleteBook(int $id, Request $request)
    {
        if($this->isCsrfTokenValid('delete'.$id, $request->get("_token"))){
//            $livre = $this->bookRepo->find($id);
            $this->bookRepo->remove($this->bookRepo->find($id), true);
//            $em = $this->getDoctrine()->getManager();
//            $em->remove($livre); // suppression du livre de la base
//            $em->flush(); // execution du flush
            $this->addFlash('success',"Le livre à bien été supprimé !!!");
            return $this->redirectToRoute('listeBooks'); // redirection vers liste livre livre
        } else {
//            return $this->redirectToRoute('');
        }
    }

    /**
     * @Route("/auteureEdit/{id}", name="auteuredit")
     * @param int $id
     * @param Request $request
     * @return void
     */
    public function authorEdit(int $id, Request $request)
    {
        ($id != -1) ? ($auteur = $this->autRepo->find($id)) : ($auteur = new Auteur());
        $form = $this->createForm(AuteurType::class,$auteur);

        return $this->render("front/form/editAuteur.html.twig",[
            "auteur" => $auteur,
            "form" => $form->createView()
        ]);
    }

}