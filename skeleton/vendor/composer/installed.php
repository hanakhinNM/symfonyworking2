<?php return array(
    'root' => array(
        'pretty_version' => 'v5.4.99',
        'version' => '5.4.99.0',
        'type' => 'project',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'reference' => NULL,
        'name' => 'symfony/skeleton',
        'dev' => true,
    ),
    'versions' => array(
        'symfony/flex' => array(
            'pretty_version' => 'v1.18.5',
            'version' => '1.18.5.0',
            'type' => 'composer-plugin',
            'install_path' => __DIR__ . '/../symfony/flex',
            'aliases' => array(),
            'reference' => '10e438f53a972439675dc720706f0cd5c0ed94f1',
            'dev_requirement' => false,
        ),
        'symfony/polyfill-ctype' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '*',
            ),
        ),
        'symfony/polyfill-iconv' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '*',
            ),
        ),
        'symfony/polyfill-php72' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '*',
            ),
        ),
        'symfony/skeleton' => array(
            'pretty_version' => 'v5.4.99',
            'version' => '5.4.99.0',
            'type' => 'project',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'reference' => NULL,
            'dev_requirement' => false,
        ),
    ),
);
